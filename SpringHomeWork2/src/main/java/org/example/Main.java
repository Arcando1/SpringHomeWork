package org.example;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

        Head head1 = context.getBean("headBean", Head.class);
        Head head2 = context.getBean("headBean", Head.class);
        Head head3 = context.getBean("headBean", Head.class);
        Hydra hydra = context.getBean("hydraBean", Hydra.class);
        hydra.setListOfHead(head1);
        hydra.setListOfHead(head2);
        hydra.setListOfHead(head3);

        System.out.println(hydra.getName() + hydra.getListOfHead());
    }
}
