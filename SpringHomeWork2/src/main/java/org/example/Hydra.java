package org.example;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;

/**
 * Описание гидры
 */
@Getter
@Setter
public class Hydra {
    @Autowired
    private Head head;
    @Value("${hydra.name}")
    private String name;

    private List<Head> listOfHead = new ArrayList<>();

    public Hydra(Head head) {
        this.head = head;
    }

    public void setListOfHead(Head head) {
        listOfHead.add(head);
    }


}
